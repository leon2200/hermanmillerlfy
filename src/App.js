import {Bar} from "./components/back/Bar";
import {useSelector} from "react-redux";
import {Container} from "./components/front/Container";
import {Routes,Link,Route} from "react-router-dom"
import {Product} from "./components/front/Product";

function App() {
    const finalStatus = useSelector(state => state?.Reducer?.startStatus)
    console.log(finalStatus)

    const singleProduct = () => {
        return(
            <div>
                <Product/>
            </div>
        )
    }


  return (
    <div className="App">
        {finalStatus && <Bar/>}

        <Routes>
            <Route path='/' element={<Container/>}></Route>
            <Route path='/product/:id' element={singleProduct()}></Route>
        </Routes>
    </div>
  );
}

export default App;

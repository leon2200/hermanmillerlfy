import {useState} from "react";
import "./Card.css"

export const Card = ({media,name,price,colorpalette}) =>{
    const color = colorpalette.split('|')
    const [hover, setHover] = useState(false)
    return(
        <div className="card">
            <img
                onMouseOver={()=>setHover(true)}
                onMouseOut={()=>setHover(false)}
                src={hover? media.split("|")[0]:media.split("|")[1]} alt=""/>
            <h2>{name}</h2>
            <span>C${price}</span>
            <div className="color">
                {color.map(color=>

                    <div
                        style={{
                        backgroundColor:color,
                        width:'10px',
                        height:'10px',
                        marginRight:'10px'
                    }}
                    >

                    </div>
                )}
            </div>

        </div>
    )
}
import "./Bar.css"
import {useDispatch, useSelector} from "react-redux";

import {closeBar} from "../../actions/action";

export const Bar = () => {
    const dispatch = useDispatch()
    return(
        <div className='topBar'>
            <div className='bar'>
                <div className="flyer">
                    <h4>Enjoy Free Shopping on Office Chairs + 0% Financing Available</h4>
                </div>

                <div className="close">
                    <a onClick={()=>dispatch(closeBar())}>x</a>
                </div>
            </div>


        </div>
    )
}
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {fetchSingleProduct} from "../../actions/action";
import {useParams} from "react-router-dom";
import {Selection} from "../back/Selection";

export const Product = (string, radix) => {
    const params = useParams()
    const productId = params.id
    const dispatch = useDispatch()
    useEffect(()=>{dispatch(fetchSingleProduct(productId))},[])
    const product = useSelector(state => state?.Reducer?.product)




    return(
        <div>
            <div>{product.price}</div>
            {!!product && <Selection
                title={product.profileCategories[0].name}
                selection={product.profileCategories[0].profileItems}
                name="one"

            />}
            {!!product && <Selection
                title={product.profileCategories[1].name}
                selection={product.profileCategories[1].profileItems}
                name="two"
                // updatePrice={updatePrice}
            />}
            {!!product && <Selection
                title={product.profileCategories[2].name}
                selection={product.profileCategories[2].profileItems}
                name="three"
            />}
            {!!product && <Selection
                title={product.profileCategories[3].name}
                selection={product.profileCategories[3].profileItems}
                name="four"
            />}
            {!!product && <Selection
                title={product.profileCategories[4].name}
                selection={product.profileCategories[4].profileItems}
                name="five"
            />}
            {!!product && <Selection
                title={product.profileCategories[5].name}
                selection={product.profileCategories[5].profileItems}
                name="six"
            />}
            {!!product && <Selection
                title={product.profileCategories[6].name}
                selection={product.profileCategories[6].profileItems}
                name="seven"
            />}
        </div>
    )
}
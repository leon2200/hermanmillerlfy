import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {fetchAllProduct} from "../../actions/action";
import {Card} from "../back/Card";
import axios from "axios";
import "./container.css"
import {Link} from "react-router-dom";

export const Container = () => {

    const dispatch = useDispatch()
    useEffect(()=> {dispatch(fetchAllProduct())},[])//add curly braces to prevent destroy error
    const productList = useSelector(state => state?.Reducer?.productList)


    return(

            <div className='cardContainer' >

                {!!productList && productList.map((item,index) =>
                    <Link to={`/product/${item.id}`} >
                    <Card
                        media={ item.media}
                        key={index}
                        name={item.name}
                        price={item.price}
                        colorpalette={item.colorPalette}
                    />
                    </Link>
                )}


            </div>



    )
}
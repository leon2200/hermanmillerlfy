import axios from "axios";

export const closeBar = () =>{
    return{
        type:'CLOSE',
        payload:false
    }
}

export const fetchAllProduct = () => async dispatch => {
    try{
        let result = await axios.get('http://api-ecommerce.mark2win.com/product')
        dispatch(
            {
                type:'FETCH_ALL_PRODUCTS',
                payload: result.data.data
            }
        )
    }catch (e) {
        console.log(e)
        dispatch({
            type:'MESSAGE',
            message: e.message
        })
    }
}

export const fetchSingleProduct = (id) => async dispatch => {
    try{
        let result = await axios.get(`http://api-ecommerce.mark2win.com/product/${id}`)
        dispatch (
            {
                type:'FETCH_SINGLE_PRODUCT',
                payload:result.data.data
            }
        )
    }catch (e) {
        console.log(e)
        dispatch({
            type:'MESSAGE',
            message: e.message
        })
    }
}


const iniState = {
    startStatus:true,
    productList:[],
    product:''
}

export const Reducer = (state = iniState, action) => {
    switch (action.type) {
        case 'CLOSE':
            return {
                ...state, startStatus: action?.payload
            }
        case 'FETCH_ALL_PRODUCTS':
            return {
                ...state, productList: action?.payload
            }
        case 'FETCH_SINGLE_PRODUCT':
            return {
                ...state, product: action?.payload
            }
        default:
            return state
    }
}